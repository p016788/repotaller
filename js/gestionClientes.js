var clientesObtenidos;

function getClientes(){

var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
var request = new XMLHttpRequest();
/*configuracion de la peticion*/
request.onreadystatechange = function(){
  /*200 respondio bien, status 4= cargado, continuar*/
  if (this.readyState == 4 && this.status == 200) {
    /*pintar el resultado en la cosnole f12*/
      console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();

  }
}

request.open("GET",url,true);
request.send();

}

function procesarClientes(){
    var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
    var JSONClientes = JSON.parse(clientesObtenidos);
    var divTabla = document.getElementById("divClientes");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");

    tabla.classList.add("table");
    //te pinta el primer renglon oscuro el siguiente en blanco y asi.
    tabla.classList.add("table-striped");

    //alert(JSONProductos.value[0].ProductName);
    for (var i = 0; i < JSONClientes.value.length; i++) {
    //  console.log(JSONProductos.value[i].ProductName);
        var nuevaFila = document.createElement("tr");
        var columnaNombre = document.createElement("td");
        columnaNombre.innerText = JSONClientes.value[i].ContactName;
        var columnaCiudad = document.createElement("td");
        columnaCiudad.innerText = JSONClientes.value[i].City;
        var columnaBandera = document.createElement("td");
        columnaBandera.innerText = JSONClientes.value[i].Country;
        var columnaBandera = document.createElement("td");
        var imgBandera = document.createElement("img");
        imgBandera.classList.add("flag");
        if(JSONClientes.value[i].Country == 'UK'){
          imgBandera.src = rutaBandera + "United-Kingdom.png";
        }  else{
          imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
        }
        columnaBandera.appendChild(imgBandera);

        nuevaFila.appendChild(columnaNombre);
        nuevaFila.appendChild(columnaCiudad);
        nuevaFila.appendChild(columnaBandera);

        tbody.appendChild(nuevaFila);
    }
    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);
}
